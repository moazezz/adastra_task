//
//  Report.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/25/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import Foundation

struct Card : Codable {
    var id: String?
    var name: String?
    var nationalPokedexNumber: Int?
    var imageUrl: String?
    var imageUrlHiRes: String?
    var types: [String]?
    var supertype: String?
    var subtype: String?
    var hp: String?
    var retreatCost: [String]?
    var convertedRetreatCost: Int?
    var number: String?
    var artist: String?
    var rarity: String?
    var series: String?
    var set: String?
    var setCode: String?
    var attacks: [Attack]?
    var resistances: [Resistance_Weakness]?
    var weaknesses: [Resistance_Weakness]?
    var imageURL:URL? {
        get {
            if let imageUrl = imageUrl{
               return URL(string: imageUrl)
            }
            return nil
        }
    }
}

struct Cards : Codable {
    var cards : [Card]?
}
struct Attack : Codable {
    var cost : [String]?
    var name : String?
    var text : String?
    var damage : String?
    var convertedEnergyCost : Int?
}

struct Resistance_Weakness : Codable {
    var type : String?
    var value : String??
}


