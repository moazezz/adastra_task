//
//  ApiManager.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/25/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import Foundation
import Alamofire



class ApiManager {
    static let shared = ApiManager()
    private init() {}
    
//    private let headers = [:]
    private let EncodingType = JSONEncoding.default
    private let QueryEncodingType = URLEncoding.default
    
    enum Apis {
        case cards
        var description: String {
            let url = "https://api.pokemontcg.io/v1"
            switch self {
            case .cards:   return url + "/cards"
            }
        }
    }
    func getCards(completion: @escaping (_ :Data?,_ error: Error?) -> Void){
        Alamofire.request(Apis.cards.description, method: .get, parameters: nil, encoding: QueryEncodingType, headers: nil).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success( _):
                completion(response.data,response.error)
            case .failure(let error):
                completion(nil,error)
                break
            }
        }
    }
}
