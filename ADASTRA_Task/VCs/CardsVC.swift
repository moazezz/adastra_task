//
//  CardsVC.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/25/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import UIKit
import SDWebImage

class CardsVC: UITableViewController {
    private var cardList : [Card]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getCards()
    }
    
    private func getCards(){
        self.showHUD()
        print("sadsda")
        ApiManager.shared.getCards {[unowned self] (data, error) in
            if let error = error {
                self.showMessage(Title: "Something went wrong!", subTitle: error.localizedDescription, theme: .error)
                return
            }
            if let data = data {
                let tuple : (model: Cards?, error: Error?)  = self.parse(data: data)
                if let error = tuple.error {
                    self.showMessage(Title: "Something went wrong!", subTitle: error.localizedDescription, theme: .error)
                    return
                }
                self.hideHUD()
                self.cardList = tuple.model?.cards
            }else{
                self.hideHUD()
                //NO DATA
            }
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardList?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! CardCell
        cell.setup(card: self.cardList?[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CardDetailsVC") as! CardDetailsVC
        vc.setCard(card: (cardList?[indexPath.row])!)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



