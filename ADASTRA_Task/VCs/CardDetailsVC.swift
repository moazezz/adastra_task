//
//  CardDetailsVC.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/28/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import UIKit

class CardDetailsVC: UIViewController {
    private var card : Card!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var rarityLbl: UILabel!
    @IBOutlet weak var seriesLbl: UILabel!
    @IBOutlet weak var artistLbl: UILabel!
    @IBOutlet weak var supertypeLbl: UILabel!
    @IBOutlet weak var subLbl: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.image.sd_setImage(with: card.imageURL, placeholderImage: #imageLiteral(resourceName: "icons8-red_yellow_cards"), options: .progressiveLoad, completed: nil)
        self.title = card.name
        self.mainLbl.text = "Name : \(card.name ?? "")"
        self.rarityLbl.text = "Rarity : \(card.rarity ?? "")"
        self.seriesLbl.text = "Series : \(card.series ?? "")"
        self.artistLbl.text = "Artist : \(card.artist ?? "")"
        self.supertypeLbl.text = "Super Type : \(card.supertype ?? "")"
        self.subLbl.text = "Sub Type : \(card.subtype ?? "")"

        // Do any additional setup after loading the view.
    }
    
    func setCard(card:Card){
        self.card = card
    }
    

}
