//
//  UIViewController.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/25/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import UIKit
import KRProgressHUD
import SwiftMessages


extension UIViewController {
    
    func parse<T: Codable>(data:Data) -> (model:T?,error:Error?){
        do {
            let model = try JSONDecoder().decode(T.self, from: data)
            return (model:model,error:nil)
        }catch(let e){
            return (model:nil,error:e)
        }
    }
    
    func showHUD() {
        KRProgressHUD.show()
        
    }
    
    func hideHUD() {
        KRProgressHUD.dismiss()
    }
    
    func showMessage(Title:String,subTitle:String,theme:Theme) {
        self.hideHUD()
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(theme)
        success.configureDropShadow()
        success.configureContent(title: Title, body: subTitle)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    func showMessageWithOption(Title:String,subTitle:String,btnTitle:String,handler:((UIButton) -> Void)?) {
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureDropShadow()
        //        success.titleLabel?.font = UIFont.init(name: "Avenir", size: 15)
        //        success.bodyLabel?.font = UIFont.init(name: "Avenir", size: 10)
        
        success.configureContent(title: Title, body: subTitle, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: btnTitle, buttonTapHandler: handler)
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .center
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    func showMessageCenterd(Title:String,subTitle:String) {
        let success = MessageView.viewFromNib(layout: .cardView)
        success.configureTheme(backgroundColor: #colorLiteral(red: 1, green: 0.5764705882, blue: 0, alpha: 1), foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        success.configureDropShadow()
        success.titleLabel?.font = UIFont.init(name: "Avenir", size: 15)
        success.bodyLabel?.font = UIFont.init(name: "Avenir", size: 10)
        //        success.button?.setTitle("Hide", for: .normal)
        success.configureContent(title: Title, body: subTitle, iconImage: nil, iconText: "", buttonImage: nil, buttonTitle: "Done") { _ in
            SwiftMessages.hide()
        }
        //        success.configureContent(title: Title, body: subTitle)
        var successConfig = SwiftMessages.defaultConfig
        successConfig.duration = .forever
        successConfig.presentationStyle = .center
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
}
