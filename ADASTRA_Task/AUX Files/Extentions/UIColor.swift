//
//  UIColor.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/25/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import UIKit

extension UIColor {
    static let myGray = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
}
