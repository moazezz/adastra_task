//
//  CardCell.swift
//  ADASTRA_Task
//
//  Created by Moaz Ezz on 11/28/19.
//  Copyright © 2019 Moaz Ezz. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var rarityLbl: UILabel!
    @IBOutlet weak var seriesLbl: UILabel!
    
    func setup(card:Card?){
        self.img.sd_setImage(with: card?.imageURL, placeholderImage: #imageLiteral(resourceName: "icons8-red_yellow_cards"), options: .progressiveLoad, completed: nil)
        self.mainLbl.text = card?.name
        self.rarityLbl.text = "Rarity : \(card?.rarity ?? "")"
        self.seriesLbl.text = "Series : \(card?.series ?? "")"
    }
}
